#!/usr/bin/python

# need for seconds calculation (could be removed with Python 2.7)
from __future__ import division
__author__ = 'fermin'

from flask import Flask, request, Response

import random
import requests

from datetime import datetime
from time import sleep


# Default arguments
port = 1029
host = '0.0.0.0'
app = Flask(__name__)


@app.route("/<device>/<id>", methods=['POST'])
def simulateActuators(device, id):
    rand = bool(random.getrandbits(1))
    now = datetime.now()
    file = open(device+":"+id+".txt", "a+")

    if rand:
        stringToWrite = str(now)+' '+device+' SUCCESS '+'\n'
        file.write(stringToWrite)
        file.close()
        return Response(status=200)
    else:
        stringToWrite = str(now)+' '+device+' ERROR '+'\n'
        file.write(stringToWrite)
        file.close()
        return Response(status=500)


@app.route("/sensors/<idSensor>/<ultraLightAttribute>/<numberOfSubmissions>", methods=['POST'])
def simulateSensors(idSensor, ultraLightAttribute, numberOfSubmissions):
    urlDevice1 = 'http://localhost:7896/iot/d?k=4jggokgpepnvsb2uv4s40d59ov&i='+idSensor
    header = {
        'Content-Type': 'text/plain'
    }

    numberOfSubmissions = int(numberOfSubmissions)
    while numberOfSubmissions > 0:
        sleep(7)
        if(ultraLightAttribute == 't'):
            rand = random.randint(0, 100)
            data = ultraLightAttribute+'|'+str(rand)
        else:
            rand = random.getrandbits(1)
            data = ultraLightAttribute+'|'+str(rand)
        print(data)
        requests.post(urlDevice1, headers=header, data=data)
        numberOfSubmissions -= 1

    return Response(status=200)


def initialize():
    app.run(host=host, port=port)


if __name__ == '__main__':
    initialize()
