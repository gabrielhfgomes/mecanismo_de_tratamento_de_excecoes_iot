#!/bin/sh

sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo apt update
sudo apt install python3
sudo apt install git
sudo apt -y install python3-pip
sudo apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
sudo snap install docker
