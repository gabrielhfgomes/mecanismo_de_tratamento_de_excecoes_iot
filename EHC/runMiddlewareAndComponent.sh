#!/bin/bash 
source bin/activate

## Pass the parameter start-ubiparking

# This command install the tools
#echo " ---------------------> 1 - Installing dependencies <--------------------- "
#bash -x installTools.sh

# This command starts the midleware
echo " ---------------------> 2 - Starting Middleware <--------------------- "
chmod +x Orion_Agent_Mongo/services
sudo Orion_Agent_Mongo/services $1

sudo chmod 777 EHC/*.txt

# This command starts the mechanism component for the context-aware exception handling
echo " ---------------------> 3 - Starting Mechanism Components <--------------------- "
chmod +x EHC/accumulator-server.py
pip3 install -r EHC/requirements.txt
python3.6 EHC/accumulator-server.py -v

