# from handlers.fireExceptionHandler import FireExceptionHandler
from handlers.exceptionHandler import ExceptionHandler
from helpers.stringToClassHelper import StringToClassHelper
import importlib


class ExceptionHandlingStrategy():

    def solveExceptionArray(self, exceptionQueue, countCalls):
        for exception in exceptionQueue:
            print(exception)

        handler = StringToClassHelper.str_to_class(
            'handlers.'+StringToClassHelper.first_lower(exception)+'Handler', exception+'Handler')

        handleLocalScope = getattr(handler, 'handleLocalScope')
        handleGroupScope = getattr(handler, 'handleGroupScope')
        handleRegionScope = getattr(handler, 'handleRegionScope')

        localBool = handleLocalScope(countCalls)

        if localBool:
            grouplocalBool = handleGroupScope()
            if grouplocalBool:
                regionBool = handleRegionScope()
                if regionBool:
                    finishHandlingString = 'Last scope achieved, the exception could not be handled!'
                    print(finishHandlingString)
                    # file = open("scenario-"+str(countCalls)+".txt","a+")
                    # file.write(finishHandlingString+'\n')
                    # file.close()

        if localBool == False:
            with open('localScopeCount.txt', 'r') as inp:
                for line in inp:
                    num = int(line)

            with open('localScopeCount.txt', 'w') as outp:
                result = str(num+1)
                outp.write(str(result))
                print('Handled Local Scope ====== {}'.format(result))
        elif grouplocalBool == False:
            with open('groupScopeCount.txt', 'r') as inp:
                for line in inp:
                    num = int(line)

            with open('groupScopeCount.txt', 'w') as outp:
                result = str(num+1)
                outp.write(result)
                print('Handled Group Scope ====== {}'.format(result))
        elif regionBool == False:
            with open('regionScopeCount.txt', 'r') as inp:
                for line in inp:
                    num = int(line)

            with open('regionScopeCount.txt', 'w') as outp:
                result = str(num+1)
                outp.write(result)
                print('Handled Region Scope ====== {}'.format(result))
        elif localBool == True and grouplocalBool == True and regionBool == True:
            with open('notHandledCount.txt', 'r') as inp:
                for line in inp:
                    num = int(line)

            with open('notHandledCount.txt', 'w') as outp:
                result = str(num+1)
                outp.write(result)
                print('Not Handled ====== {}'.format(result))
