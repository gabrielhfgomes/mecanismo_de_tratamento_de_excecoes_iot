CONTEXTELEMENTS = ['temperature', 'statusParkSpace']
FIREEXCEPTIONVALUE = 50
STATUSPARKSPACE = '1'
IDLOCATION1 = 'urn:ngsi-ld:Park:001'
IDLOCATION2 = 'urn:ngsi-ld:Park:002'
NAMELOCATION = 'refPark'
# The header to connect the middleware API
HEADER = {
    'Content-Type': 'application/json',
    'fiware-service': 'openiot',
    'fiware-servicepath': '/'
}
HEADERGET = {
    'fiware-service': 'openiot',
    'fiware-servicepath': '/'
}

TIMEWAITINGEXCEPTIONS = 0
