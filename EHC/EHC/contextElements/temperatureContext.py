from contextElements.contextElement import ContextElement
from contextualException import ContextualException
import const


class TemperatureContext(ContextElement):
    def checkContextualException(self):

        print("Checking Contextual Exception")
        if (float(self.value) >= const.FIREEXCEPTIONVALUE) and self.verifyLocation:
            raise ContextualException("Fire")
