from contextElements.contextElement import ContextElement
from contextualException import ContextualException
import const


class StatusParkSpaceContext(ContextElement):
    def checkContextualException(self):
        print("Checking Contextual Exception")
        if (self.value == const.STATUSPARKSPACE) and self.verifyLocation and (not self.hasAvailableParkSpaces()):
            raise ContextualException("BusySpace")

    def hasAvailableParkSpaces(self):
        parkSpaces = self.callRequestConsult(
            'Spacesensor', const.IDLOCATION1)

        for parkSpace in parkSpaces:
            if(parkSpace['statusParkSpace'] != const.STATUSPARKSPACE):
                return True

        return False
