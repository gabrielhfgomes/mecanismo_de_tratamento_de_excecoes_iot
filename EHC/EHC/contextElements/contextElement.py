import const
from contextualException import ContextualException
from handlers.apiRequest import ApiRequest


class ContextElement():
    def __init__(self, element, value, location):
        print('ContextElement Instance')
        self.location = location
        self.element = element
        self.value = value

    def verifyLocation():
        if(self.location == const.NAMELOCATION):
            return True

    def callRequestConsult(self, typeEntity, location, resource=''):
        apirequest = ApiRequest()
        data = apirequest.send('http://localhost:1026/v2/entities/?q='+const.NAMELOCATION +
                               '=='+location+'&options=keyValues&type='+typeEntity, 'get')

        r = []
        for device in data:
            if(resource == 'id'):
                r.append(device['id'][-3:])
            else:
                r.append(device)

        return r
