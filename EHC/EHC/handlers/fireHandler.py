from handlers.apiRequest import ApiRequest
from handlers.exceptionHandler import ExceptionHandler
import const
from devices import Devices


class FireHandler(ExceptionHandler):
    countCalls = 0

    def handleLocalScope(self, countCalls):
        self.countCalls = countCalls
        devices = []
        sprinklers = Devices('Sprinkler', ['001'], ['on'])
        devices.append(sprinklers)
        return self.handle(devices, 'Local', self.countCalls)

    def handleGroupScope(self):
        devices = []
        sprinklers = Devices('Sprinkler', ['001', '002'], ['on', 'on'])
        soundAlarm = Devices('SoundAlarm', ['001'], ['on'])
        devices.append(sprinklers)

        return self.handle(devices, 'Group', self.countCalls)

    def handleRegionScope(self):
        idsSoundAlarms = self.callRequestConsult(
            'SoundAlarm', const.IDLOCATION1, 'id')
        idPhone = self.callRequestConsult(
            'SoundAlarm', const.IDLOCATION1, 'id')

        devices = []
        soundAlarm = Devices('SoundAlarm', idsSoundAlarms, ['on'])
        phone = Devices('Phone', idPhone, ['call'])

        devices.append(soundAlarm)
        devices.append(phone)
        return self.handle(devices, 'Region', self.countCalls)
