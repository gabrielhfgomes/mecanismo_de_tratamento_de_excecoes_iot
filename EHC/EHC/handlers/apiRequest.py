import requests
import json
import const
from time import sleep


class ApiRequest:

    def patch(url, data=None, **kwargs):
        return request('patch', url,  data=data, **kwargs)

    def send(self, url, method, data=''):
        if method == 'patch':
            r = requests.patch(url, json=data, headers=const.HEADER)
            return r
        elif method == 'get':
            response = requests.get(url, headers=const.HEADERGET)
            data = response.json()
            return data

    def get(self, url, command):
        # sleep(1)
        response = requests.get(url, headers=const.HEADERGET)
        return response.json()[command+'_status']['value']
