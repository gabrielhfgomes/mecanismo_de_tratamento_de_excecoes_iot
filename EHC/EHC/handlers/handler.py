from handlers.apiRequest import ApiRequest


class Handler():

    def handle(self, exception):
        try:
            method = getattr(self, exception)
            print(method)
        except AttributeError:
            raise NotImplementedError("Error")

    def openShutterHandler(self, data):
        apirequest = ApiRequest()
        data = {
            'open': {'type': 'command', 'value': ''}
        }
        handlingStatus = apirequest.send(
            'http://localhost:1026/v2/entities/urn:ngsi-ld:Shutter:001/attrs', 'patch', data)
        return handlingStatus

    def openShutterAndWindowsHandler(self):
        return 0
