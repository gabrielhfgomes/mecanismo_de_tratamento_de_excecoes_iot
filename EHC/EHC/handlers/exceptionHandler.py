import const
from datetime import datetime
from handlers.apiRequest import ApiRequest


class ExceptionHandler(object):

    def handleLocalScope(self):
        # Exemplo abrir o Shutter, encapsular a chamada do device de tratamento
        raise NotImplementedError(
            'subclasses must implement handleLocalScope()!')

    def handleGroupScope(self):
        raise NotImplementedError(
            'subclasses must implement handleGroupScope()!')

    def handleRegionScope(self):
        raise NotImplementedError(
            'subclasses must implement handleRegionScope()!')

    def callRequestConsult(self, typeEntity, location, resource=''):
        apirequest = ApiRequest()
        data = apirequest.send('http://localhost:1026/v2/entities/?q='+const.NAMELOCATION +
                               '=='+location+'&options=keyValues&type='+typeEntity, 'get')

        r = []
        for device in data:
            if(resource == 'id'):
                r.append(device['id'][-3:])
            else:
                r.append(device)

        return r

    def iterateOverResponses(self, responses):
        for response in responses:
            if response != False:
                return True

        return False

    def handle(self, devices, scope, countCalls):
        now = datetime.now()

        responses = []
        for device in devices:
            #file = open("scenario-"+str(countCalls)+".txt","a+")
            startString = str(now) + ' ------> ' + scope.upper() + \
                ' SCOPE DEVICES ' + device.devices_type + ' <------ '
            print(startString)
            # file.write(startString+'\n')
            for i in range(0, len(device.ids)):
                idHandler = 'urn:ngsi-ld:' + \
                    device.devices_type + ':' + device.ids[i]
                handlingResponse = self.callRequestHandler(
                    device.commands[i], idHandler)
                if handlingResponse == 'ERROR':
                    responses.append(True)
                else:
                    responses.append(False)

        #endString = str(now) + ' ------> END '+ scope.upper() +' SCOPE <------ '

        # print(endString)
        # file.write(endString+'\n')
        # file.close()
        return self.iterateOverResponses(responses)

    def callRequestHandler(self, command, idHandler):
        apirequest = ApiRequest()
        data = {
            command: {'type': 'command', 'value': ''}
        }
        apirequest.send('http://localhost:1026/v2/entities/' +
                        idHandler+'/attrs', 'patch', data)
        statusHandling = apirequest.get(
            'http://localhost:1026/v2/entities/'+idHandler, command)
        return statusHandling
