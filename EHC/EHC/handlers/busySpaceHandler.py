from handlers.apiRequest import ApiRequest
from handlers.exceptionHandler import ExceptionHandler
import const
from devices import Devices


class BusySpaceHandler(ExceptionHandler):
    countCalls = 0

    def handleLocalScope(self, countCalls):
        self.countCalls = countCalls
        devices = []
        lightPanels = Devices('LightPanel', ['001'], ['right'])
        devices.append(lightPanels)

        return self.handle(devices, 'Local', self.countCalls)

    def handleGroupScope(self):
        # curl -X GET   'http://localhost:1026/v2/entities/?q=refPark==urn:ngsi-ld:Park:001&options=keyValues&type=Spacesensor'   -H 'fiware-service: openiot'   -H 'fiware-servicepath: /' | python -m json.tool

        # Search in the park if there is a empty park space(this could be a call for the Web Service logic of the parking.
        spaceSensors = self.callRequestConsult(
            'Spacesensor', const.IDLOCATION1)
        # Take the sensors and check the one that is avaiable
        # Call webservice for the smallest routes that return the panels
        for sensor in spaceSensors:
            if(sensor['statusParkSpace'] != const.STATUSPARKSPACE):
                print(
                    'Call the microservice to the get the shortest path to the sensor with id = ' + sensor['id'])

        devices = []
        lightPanels = Devices('LightPanel', ['002', '003'], ['right', 'left'])

        devices.append(lightPanels)

        return self.handle(devices, 'Group', self.countCalls)
        # Call various lightpanels inside the park to the closest exit to go to the other park

    def handleRegionScope(self):
        # Search in all region1 parks for a empty park space
        # Call various lightpanels inside the park to the closest exit to go to the other park
        locations = [const.IDLOCATION1, const.IDLOCATION2]

        for location in locations:
            spaceSensors = self.callRequestConsult('Spacesensor', location)
            for sensor in spaceSensors:
                if(sensor['statusParkSpace'] != const.STATUSPARKSPACE):
                    print(
                        'Call the microservice to the get the shortest path to the sensor with id = ' + sensor['id'])

        devices = []
        lightPanels = Devices('LightPanel', ['004', '005', '006'], [
                              'right', 'left', 'right'])
        devices.append(lightPanels)

        return self.handle(devices, 'Region', self.countCalls)
