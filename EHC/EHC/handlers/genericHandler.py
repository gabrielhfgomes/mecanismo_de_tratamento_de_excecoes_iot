from handlers.apiRequest import ApiRequest
from handlers.exceptionHandler import ExceptionHandler
import const
from devices import Devices


class GenericHandler(ExceptionHandler):
    countCalls = 0

    def handleLocalScope(self, countCalls):
        self.countCalls = countCalls
        devices = []
        genericDevice = Devices('Genericdevice', ['001'], ['right'])
        devices.append(genericDevice)

        return self.handle(devices, 'Local', self.countCalls)

    def handleGroupScope(self):
        genericSensors = self.callRequestConsult(
            'Genericsensor', const.IDLOCATION1)

        for sensor in genericSensors:
            if(sensor['statusParkSpace'] != const.STATUSPARKSPACE):
                print(
                    'Call the microservice to the get the shortest path to the sensor with id = ' + sensor['id'])

        devices = []
        genericDevices = Devices(
            'Genericdevice', ['002', '003'], ['right', 'left'])

        devices.append(genericDevices)

        return self.handle(devices, 'Group', self.countCalls)

    def handleRegionScope(self):
        locations = [const.IDLOCATION1, const.IDLOCATION2]
        for location in locations:
            genericSensors = self.callRequestConsult('Genericsensor', location)
            for sensor in genericSensors:
                if(sensor['statusParkSpace'] != const.STATUSPARKSPACE):
                    print(
                        'Call the microservice to the get the shortest path to the sensor with id = ' + sensor['id'])

        devices = []
        genericDevices = Devices('Genericdeivce', ['004', '005', '006'], [
                                 'right', 'left', 'right'])
        devices.append(genericDevices)
        return self.handle(devices, 'Region', self.countCalls)
