import importlib


class StringToClassHelper():

    @staticmethod
    def first_upper(s):
        if len(s) == 0:
            return s
        else:
            return s[0].upper() + s[1:]

    @staticmethod
    def first_lower(s):
        if len(s) == 0:
            return s
        else:
            return s[0].lower() + s[1:]

    @staticmethod
    def str_to_class_context_element(module_name, class_name, element, value, location):
        logging = ''
        try:
            module_ = importlib.import_module(module_name)
            try:
                class_ = getattr(module_, class_name)(element, value, location)
                print(class_)
            except AttributeError:
                logging.error('Class does not exist')
        except ImportError:
            logging.error('Module does not exist')
        return class_ or None

    @staticmethod
    def str_to_class(module_name, class_name):
        logging = ''
        try:
            module_ = importlib.import_module(module_name)
            try:
                class_ = getattr(module_, class_name)()
            except AttributeError:
                logging.error('Class does not exist')
        except ImportError:
            logging.error('Module does not exist')
        return class_ or None
